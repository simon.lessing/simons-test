import React, { Children } from "react";
import styled from "styled-components";
import Badge from "./Badge";

export default function Button(props) {
  const { title, icon, iconRight } = props;
  console.log(props.iconRight);
  {
    return (
      <Wrapper {...props}>
        {props.children && (
          <div>
            <Icon>{props.children}</Icon>{" "}
          </div>
        )}
        <Title>{title}</Title>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.iconRight ? "row;" : "row-reverse")};
  gap: 8px;
  padding: 0 12px;
  height: 32px;
  background: #f9f9f9;
  align-items: center;
  position: relative;
  border: 1px solid #dddde7;
  border-radius: 5px;
  color: #535358;
  box-sizing: border-box;

  :hover {
    background: #ffffff;
    box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.06);
    cursor: pointer;
  }
  :active {
    box-shadow: none;
    border-color: #ccc;
    transform: scale(0.95);
    transition: 50ms;
  }
`;
const Title = styled.p`
  font-size: 12px;
`;

const Icon = styled.div`
  display: flex;
  align-items: center;
`;
