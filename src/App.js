import styled from "styled-components";
import Badge from "./Badge";
import Note from "./Note";
import OneTwoBadgesWrapper from "./OneTwoBadgesWrapper";
import Button from "./Button";
import NoteIcon from "./note.svg";
import Chevron from "./chevron.svg";

export default function App() {
  return (
    <div>
      <Wrapper>
        <Badge alerts="3" prio="high"></Badge>sla
        <Button title="Clear alarms"></Button>
        <Button title="Clear alarms">
          <img src={Chevron} />
        </Button>
        <Button title="Add note" iconRight>
          <img src={NoteIcon} />
        </Button>
        <Note></Note>
      </Wrapper>
    </div>
  );
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
  justify-content: center;
  align-items: center;
  background-color: #f7f7f7;
`;

const Icon = styled.div`
  width: 100px;
  height: 100px;
  background-color: red;
`;
