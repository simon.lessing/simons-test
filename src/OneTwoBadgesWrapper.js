import React, { useState } from "react";
import styled from "styled-components";
import Badge from "./Badge";
import { motion } from "framer-motion";

export default function OneTwoBadgesWrapper(props) {
  const { alerts, prio, total } = props;
  const [isOpen, setIsOpen] = useState(false);
  const itemVariants = {
    visible: { opacity: 1, x: 0 },
    hidden: { opacity: 0, x: 100 },
  };
  let TotalBadge;

  if (total > 0) {
    TotalBadge = (
      <Badge
        style={{ marginLeft: "-2px" }}
        alerts={total}
        prio="low"
        overlap
      ></Badge>
    );
  }
  {
    return (
      <Wrapper
        as={motion.div}
        initial="hidden"
        animate="visible"
        variants={itemVariants}
        whileHover={{ scale: 1.2 }}
        whileTap={{ scale: 0.8 }}
        onClick={() => setIsOpen(true)}
      >
        <Badge alerts={alerts} prio={prio}></Badge>
        {isOpen ? TotalBadge : ""}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  width: 100px;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: -3px;
  background-color: yellow;
`;
