import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  font-size: 11px;
  line-height: 150%;
  justify-content: center;
  align-items: center;
  width: 16px;
  height: 16px;

  border-radius: 16px;
  color: white;
  font-weight: 600;

  ${(props) => {
    switch (props.prio) {
      case "high":
        return "background-color: #FF3B30;";
      case "low":
        return "background-color: #535358; box-shadow: 0 0 0 1px #f7f7f7;";
    }
  }}
`;

export default function Badge(props) {
  const { alerts, prio } = props;
  {
    return (
      <div>
        <Wrapper {...props}>{alerts}</Wrapper>
      </div>
    );
  }
}
