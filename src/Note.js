import React, { useState } from "react";
import styled from "styled-components";
import HintIcon from "./pen.svg";
import Button from "./Button";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";

export default function Note() {
  const [charsWritten, setCharsWritten] = useState(false);
  const [input, setInput] = useState(""); // '' is the initial state value

  return (
    <Wrapper>
      <Input value={input} onInput={(e) => setInput(e.target.value)}></Input>
      {console.log(input)}

      {!input ? (
        <Hints>
          <img src={HintIcon}></img>
          <HintText>Notes are shown to everyone in your team</HintText>
          <Button>Mark as read</Button>
        </Hints>
      ) : (
        <BottomWrapper>
          <Icons>
            <Icon>􀋙</Icon>
            <Tooltip
              TransitionComponent={Fade}
              TransitionProps={{ timeout: 200 }}
              title="Set as important"
              arrow
              placement="top"
            >
              <Icon2></Icon2>
            </Tooltip>
            <Icon>􀈑</Icon>
          </Icons>
          <Saved>Saved Today 14:23</Saved>
        </BottomWrapper>
      )}
    </Wrapper>
  );
}

const Input = styled.textarea`
  width: 100%;
  height: 100%;
  padding: 16px;
  font-size: 14px;
  box-sizing: border-box;
  border: 0;
  resize: none;
  margin: 0;

  &:focus {
    border: 0;
    outline: none;
    resize: none;
  }
`;

const Hints = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  gap: 16px;
  transition: 1s;
`;

const BottomWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding: 0px 16px;
  box-sizing: border-box;
`;

const Icons = styled.div`
  display: flex;
  gap: 8px;
`;

const Icon = styled.div`
  font-family: SF Pro Display;
`;

const Icon2 = styled.div`
  font-family: SF Pro Display;
  &:after {
    content: "􀁞";
  }
  &:hover:after {
    content: "􀁟";
    color: #ffcc00;
    cursor: pointer;
  }
`;

const Saved = styled.div`
  width: 64px;
  height: 36px;
  font-size: 11px;
  line-height: 164.5%;

  text-align: right;

  color: #535358;

  opacity: 0.5;
`;

const Wrapper = styled.div`
  gap: 16px;
  width: 268.8px;
  height: 253.82px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding-bottom: 16px;
  align-items: center;
  box-sizing: border-box;

  background: white;
  box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.06);
  border-radius: 6px;
`;

const HintText = styled.div`
  width: 169.01px;
  height: 36px;

  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 129.5%;

  display: flex;
  align-items: center;
  text-align: center;

  color: #535358;

  opacity: 0.5;
`;
